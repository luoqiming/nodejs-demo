/**
 * Created by luoqiming on 2016/12/29.
 */
var express = require('express');
var moment = require('moment');
var router = express.Router();

router.get('/', function (req, res, next) {
    res.render('tool', {title:'Format'});
})

router.get('/format', function (req, res, next) {
    var source = req.query.source;
    var formatDate = moment("/Date("+ source +")/").format('YYYY-MM-DD HH:mm:ss');
    var obj = {result: formatDate};
    res.json(obj);
});

module.exports = router;
