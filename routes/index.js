var express = require('express');
//加载模块
var db = require('./db');
var util = require('./util');
var es = require('./countES');
var router = express.Router();
//创建db对象
//var dbUtil = new db.dbUtil();

//var chart_data = {key:['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'], value:[10, 52, 200, 334, 390, 330, 220]};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Chart'});
});

router.get('/chart_bar', function (req, res, next) {

  var endDateKey = req.query.endDateKey;
  var query = req.query.mongo_query;
  var time;
  var limit = 20;
  if (util.isEmpty(endDateKey)) { //等于空
    console.log('endDateKey为空');
    time = new Date().getTime() - 1200000;
  } else {
    var oldDate = new Date().getYear() + "/" + endDateKey;
    time = Date.parse(oldDate) / 1000;
    limit = 1;
  }
  var json = eval('(' + query + ')');
  console.log(json);
  //query = JSON.parse(query);
  //var query = {'count_time':{$gt: time}};
  db.getCountChart(json, limit, function (data) {
    console.log(data);
    res.json(data);
  });
});

router.get('/count_es', function (req, res, next) {
  var esIndex = req.query.esIndex;
  var searchkey = req.query.searchkey;
  /*es.serachContent(esIndex, function (data) {
    res.json(data);
  });
  es.searchKey(esIndex, searchkey, function (data) {
    console.log(JSON.stringify(data));
    //res.json(data);
  });*/
  es.count(esIndex, function (data) {
    console.log(data);
  });
});

module.exports = router;
