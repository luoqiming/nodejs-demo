
function formateArray(dataValue) {
    var x = [];
    var data = [];
    if (dataValue != null) {
        for (var i = 0; i < dataValue.length; i++) {
            x[i] = dataValue[i].countKey;
            data[i] = dataValue[i].count_value;
        }
    }
    var formatDate = {xAxis:x, data: data};
    return formatDate;
}

function myCharts(option, domId) {
    var myChart = echarts.init(document.getElementById(domId));
    myChart.setOption(option);
}


function pie(xAxis, data) {
    var option = {
        title : {
            text: 'BSMSSAGE根据ID统计图',
            subtext: '单位：毫秒',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            show: false,
            orient: 'vertical',
            left: 'left',
            data: xAxis
        },
        series : [
            {
                name: '时间差',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:data,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    }
    return option;
}


function bar(xAxis, data) {
    var option = {
        title: {
            text:'根据BSMESSAGE_ID统计',
            subtext: '单位:毫秒'
        },
        color: ['#3398DB'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            },
            formatter: "{a}<br/>第{b}步:{c}毫秒"
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                data : xAxis,
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'统计',
                type:'bar',
                barWidth: '60%',
                data:data
            }
        ]
    };
    return option;
}


/**
 * 大面积折线图
 * @param xaxisDate
 * @param data
 * @returns {{tooltip: {trigger: string, position: option.tooltip.position}, title: {left: string, text: string, subtext: string}, toolbox: {feature: {dataZoom: {yAxisIndex: string}, restore: {}, saveAsImage: {}}}, xAxis: {type: string, boundaryGap: boolean, data: *}, yAxis: {type: string, boundaryGap: *[]}, dataZoom: *[], series: *[]}}
 */
function areaSimple(xaxisDate, data) {
    var option = {
        tooltip: {
            trigger: 'axis',
            position: function (pt) {
                return [pt[0], '10%'];
            }
        },
        title: {
            left: 'center',
            text: '历史电文数据统计',
            subtext: '24小时统计'
        },
        toolbox: {
            feature: {
                dataZoom: {
                    yAxisIndex: 'none'
                },
                restore: {},
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: xaxisDate
        },
        yAxis: {
            type: 'value',
            boundaryGap: [0, '100%']
        },
        dataZoom: [{
            type: 'inside',
            start: 0,
            end: 10
        }, {
            start: 0,
            end: 10,
            handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
            handleSize: '80%',
            handleStyle: {
                color: '#fff',
                shadowBlur: 3,
                shadowColor: 'rgba(0, 0, 0, 0.6)',
                shadowOffsetX: 2,
                shadowOffsetY: 2
            }
        }],
        series: [
            {
                name:'电文数据',
                type:'line',
                smooth:true,
                symbol: 'none',
                sampling: 'average',
                itemStyle: {
                    normal: {
                        color: 'rgb(255, 70, 131)'
                    }
                },
                areaStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgb(255, 158, 68)'
                        }, {
                            offset: 1,
                            color: 'rgb(255, 70, 131)'
                        }])
                    }
                },
                data: data
            }
        ]
    };
    return option;
}

/**
 * 动态柱状图
 * @param xAxisData
 * @param data
 * @returns {{title: {text: string}, tooltip: {trigger: string}, legend: {data: string[]}, toolbox: {show: boolean, feature: {saveAsImage: {}}}, xAxis: {type: string, boundaryGap: boolean, data: *}, yAxis: {type: string, scale: boolean, name: string, min: number, boundaryGap: number[]}, series: *[]}}
 */
function dynamicBar(xAxisData, data) {
    var option = {
        title: {
            text: 'UECP电文消息动态'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data:['电文消息']
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: true,
            data: xAxisData
        },
        yAxis: {
            type: 'value',
            scale: true,
            name: '数据量',
            //max: 1200,
            min: 0,
            boundaryGap: [0.2, 0.2]
        },
        series: [
            {
                name:'电文消息',
                type:'bar',
                data: data
            }
        ]

    };
    return option;
}

/**
 * 动态折线图
 * @param xAxisData
 * @param data
 * @returns {{title: {text: string}, tooltip: {trigger: string}, toolbox: {show: boolean, feature: {saveAsImage: {}}}, xAxis: {boundaryGap: boolean, data: *}, yAxis: {type: string, boundaryGap: *[]}, series: *[]}}
 */
function dynamincOneLine(xAxisData, data) {
    var option = {
        title: {
            text: '电文包长动态数据'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data:['电文包长']
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            boundaryGap: true,
            data: xAxisData
        },
        yAxis: {
            type: 'value',
            boundaryGap: [0, '100%'],
        },
        series: [{
            name: '电文包长',
            type: 'line',
            data: data
        }]
    };
    return option;
}

