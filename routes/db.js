/**
 * Created by luoqiming on 2016/12/5.
 */
var db = require('mongoskin').db('mongodb://10.60.63.202:27017/sample');

var dbUtil = {
    /**
     * 根据条件查询chartBsmessage
     * @param param
     */
    getCountChart : function (param, limit, callback) {
        console.log('param:' + param + "#limit:" + limit);
        db.collection('chartBsmessage').find(param,{'count_time':1,'count_value':1,'countKey':1}).limit(limit).sort({'id':1}).toArray(function(err, result) {
            if (err) throw err;
            //console.log(result);
            callback(result);
        });
    }
}
//类是创建对象,需要铜鼓new才能使用
//exports.dbUtil = dbUtil;
//外部引用方式
//var db = require('./db');
//创建db对象
//var dbUtil = new db.dbUtil();
//dbUtil.getCountChart(xx, xx, xx);

//缺点 ,容易对node已存在的模块冲突
//类是创建全局对象,直接引用就可使用对象中的方法
module.exports = dbUtil;
//引用对象
//var db = require('./db');
//dbUtil.getCountChart(xx, xx, xx);




