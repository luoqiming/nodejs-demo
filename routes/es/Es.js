/**
 * Created by luoqiming on 2016/12/15.
 */
var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: ['10.60.17.40:9200','10.60.17.41:9200']
    //log: 'trace'
});

exports.es = (function () {
    var searchId = function (esIndex, sort, sizeNum, callback) {
        client.search({
            index: esIndex,
            //q:condition,
            body:{
                "size": 0,
                "query": {
                    "bool": {
                        "must": [
                            {
                                "query_string": {
                                    "query": "*",
                                    "analyze_wildcard": true
                                }
                            },
                            {
                                "range": {
                                    "@timestamp": {
                                        "gte": 1481731200000,
                                        "lte": 1481817599999,
                                        "format": "epoch_millis"
                                    }
                                }
                            }
                        ],
                        "must_not": []
                    }
                },
                "aggs": {
                    "groupId": {
                        "terms": {
                            "field": "doc.id.keyword",
                            "size": sizeNum,
                            "order": {
                                "_term": sort
                            }
                        }
                    }
                }
            }
        }, function (error, response) {
            //callback(response.aggregations.groupId.buckets);
            callback(response.aggregations.groupId.buckets);
        });
    };

    var searchEsById = function (esIndex, key, callback) {
        console.log("索引:%s, 关键词:%s", esIndex, key);
        client.search({
            index: esIndex, //索引
            body: {
                "query" : {
                    "term" : { "doc.id.keyword" : key }
                },
                "sort": [{"doc.startTime":{"order":"asc"}}]
            }
        }, function (error, response) {
            if (error) {
                console.error(error.message);
            } else {
                var hits = response.hits.hits;
                console.log('索引%s查询结果%s', esIndex, hits);
                //query_array.push(charObj);
                //console.log("全部数据:");
                //console.log(JSON.stringify(query_array));
                callback(hits);
            }
        });
    };

    var formatData = function (data, callback) {
        if (data.length > 0) {
            //排序
            //data.sort(sort_Es)
            var array = new Array();
            //console.log(JSON.stringify(data));

            //过滤掉不需要的字段
            for (var i = 0; i < data.length; i++) {
                var startTime = data[i]._source.doc.startTime;
                var id = data[i]._source.doc.id;
                var content = data[i]._source.doc.content;
                var obj = {id:id,startTime: startTime, content: content};
                array.push(obj);
            }
            console.log('过滤后的字段:')
            console.log(JSON.stringify(array));

            //第一步 步骤
            console.log("第一步时间差:", difference(array[0].startTime,array[1].startTime));
            console.log("第二步时间差:", difference(array[0].startTime,array[2].startTime));
            console.log("第三步时间差:", difference(array[2].startTime,array[3].startTime));
            console.log("第四步时间差:", difference(array[3].startTime,array[4].startTime));
            console.log("第五步时间差:", difference(array[4].startTime,array[5].startTime));

            var obj = new Array();
            obj.push({"countKey":"xd-to-ibmmq","count_value":difference(array[0].startTime,array[1].startTime)});
            obj.push({"countKey":"xd-to-rabbit","count_value":difference(array[0].startTime,array[2].startTime)});
            obj.push({"countKey":"rabbit-to-app","count_value":difference(array[2].startTime,array[3].startTime)});
            obj.push({"countKey":"app-search-mogodb","count_value":difference(array[3].startTime,array[4].startTime)});
            obj.push({"countKey":"app-save-mogodb","count_value":difference(array[4].startTime,array[5].startTime)});
            callback(obj);
        }
    };
    var difference = function (val_1, val_2) {
        return val_2 - val_1;
    }
    return {
        searchId: searchId,
        searchEsById:searchEsById,
        formatData:formatData,
        difference:difference
    }
})();
