/**
 * Created by luoqiming on 2016/12/6.
 */

var Util = {
    isEmpty: function (str) {
        return str == null || str == undefined || str.replace(/\s/g, '') == '';
    }
}

//把本JS模块化
module.exports = Util;