/**
 * Created by luoqiming on 2016/12/15.
 */
var esObj = require('./Es');
var module_util = require('../util')
var async = require('async');
var es = esObj.es;

var esIndex = "uecp_*_trace_bsmessage*";
var sort = "desc";
var arrayAll = new Array();
var searchLength = 0;
var sizeNum = 100;

var util = (function () {
    var countEs = function (callback) {
        es.searchId(esIndex, sort, sizeNum, function (data) {
            async.eachSeries(data, function (item, callback) {
                es.searchEsById(esIndex, item.key, function (data) {
                    es.formatData(data, function (data) {
                        pushArray(data);
                        callback(null);
                    })
                });

            }, function (err) {
                if(err) console.log(err);
                callback(true);
            });
        });
    };
    var pushArray = function (data) {
        console.log("存入数组中的数据:", JSON.stringify(data));
        arrayAll.push(

        );
        //callback(data);
    };
    var countStepNum = function (data, callback) {
        var stopCountNum = [{step:[]},{step:[]},{step:[]},{step:[]},{step:[]}];
        if (data != null && data.length > 0) {
            console.log("开始汇总每步;");
            async.eachSeries(data, function (item, callback) {
                stopCountNum[0].step.push(item[0].count_value);
                stopCountNum[1].step.push(item[1].count_value);
                stopCountNum[2].step.push(item[2].count_value);
                stopCountNum[3].step.push(item[3].count_value);
                stopCountNum[4].step.push(item[4].count_value);
                callback(null);
            }, function (err) {
                console.log("汇总完成");
                callback(stopCountNum);
            });
        }
    };
    var sumStep = function (data, callback) {
        console.log("sum计算开始");
        var sum = 0;
        async.eachSeries(data, function (item, callback) {
            sum = sum + item;
            callback(null);
        }, function (err) {
            callback(sum);
        });
        console.log("sum计算结束");
    };
    var maxStep = function (data, callback) {
        console.log("maxStep:", JSON.stringify(data));
        console.log("max计算开始");
        callback(Math.max.apply(null, data));
        console.log("max计算结束");
    };
    var minStep = function (data, callback) {
        console.log("minStep:", JSON.stringify(data));
        callback(Math.min.apply(null, data));
        console.log("min计算结束");
    };
    var avgStep = function (data, callback) {
        console.log("avg计算开始");
        callback(data/sizeNum);
        console.log("avg计算结束");
    }

    var showData = function (callback) {
        countEs(function (data) {
            console.log('执行完了:');
            countStepNum(arrayAll, function (data) {
                console.log("每步:", JSON.stringify(data));
                var countAll =[];
                countAll.push({"all":arrayAll})
                var i = 1;
                async.eachSeries(data, function (item, callback) {
                    var countObj = new Object();
                    sumStep(item.step, function (data) {
                        countObj.sum = data;
                    });
                    maxStep(item.step, function (data) {
                        countObj.max = data;
                    })
                    minStep(item.step, function (data) {
                        countObj.min = data;
                    });
                    avgStep(countObj.sum, function (data) {
                        countObj.avg = data;
                    })
                    countAll.push(countObj);
                    callback(null);
                }, function (err) {
                    if (err) console.error(err);
                    console.log("统计最大最小:");
                    console.log(JSON.stringify(countAll));
                    callback(countAll);
                });
            });
        });
    }
    return {
        countEs : countEs,
        countStepNum: countStepNum,
        sumStep:sumStep,
        maxStep:maxStep,
        minStep:minStep,
        avgStep:avgStep,
        showData: showData
    }
})();

console.log("执行:");
util.showData(function (data) {

});